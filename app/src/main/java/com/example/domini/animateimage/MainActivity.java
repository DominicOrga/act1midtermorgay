package com.example.domini.animateimage;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView mImageA;
    private ImageView mImageB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageA = (ImageView) findViewById(R.id.image_a);
        mImageB = (ImageView) findViewById(R.id.image_b);

        setImageAListener();
    }

    private void setImageAListener() {
        mImageA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageA.getAlpha() == 0)
                    fadeImage(mImageB, mImageA);
                else
                    fadeImage(mImageA, mImageB);
            }
        });
    }

    private void fadeImage(ImageView a, ImageView b) {
        a.animate()
                .rotationBy(3600)
                .alpha(0f)
                .setDuration(1000)
                .start();

        b.animate()
                .rotationBy(3600)
                .alpha(1f)
                .setDuration(1000)
                .start();
    }
}
